﻿namespace BinaryTask1.Models
{
    public struct UserStruct
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int ProjectTaskCount { get; set; }
        public int UnfinishedTaskCount { get; set; }
        public BinaryTask1.Models.Task LongestTask { get; set; }

        public override string ToString()
        {
            return $"{User}\n{LastProject}\n{ProjectTaskCount}\n{UnfinishedTaskCount}\n{LongestTask}";
        }
    }
}