﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTask1.Services
{
    class HttpService
    {
        private HttpClient _httpClient;
        private Uri _baseUri = new Uri("https://localhost:44322/");

        private Task<HttpResponseMessage> _projectTaskCountByUserAsync;
        private Task<HttpResponseMessage> _tasksByUserLess45Async;
        private Task<HttpResponseMessage> _taskNamesByUserDone;
        private Task<HttpResponseMessage> _teamsWithUsersOlder10;
        private Task<HttpResponseMessage> _usersWithTasks;
        private Task<HttpResponseMessage> _userStruct;
        private Task<HttpResponseMessage> _projectStructs;

        public HttpService(HttpClient httpClient)
        {
            _httpClient = httpClient;
            
        }

        public async void SendData()
        {
            var content = new StringContent(
                "{\r\n  " +
                "\"id\": 0,\r\n  " +
                "\"authorId\": 0,\r\n  " +
                "\"teamId\": 0,\r\n  " +
                "\"name\": \"ProjectName1\",\r\n  " +
                "\"description\": \"DescriptionForProject1\",\r\n  " +
                "\"deadline\": \"2021-06-29T16:00:47.492Z\",\r\n  " +
                "\"createdAt\": \"2021-06-29T16:00:47.492Z\"\r\n}", 
                Encoding.UTF8, "application/json");

            await _httpClient.PostAsync(_baseUri + "api/Projects", content);

            content = new StringContent(
                "{\r\n  " +
                "\"id\": 0,\r\n  " +
                "\"projectId\": 0,\r\n  " +
                "\"performerId\": 0,\r\n  " +
                "\"name\": \"TaskName1\",\r\n  " +
                "\"description\": \"DescriptionForTask1\",\r\n  " +
                "\"state\": 0,\r\n  " +
                "\"createdAt\": \"2021-06-29T16:00:54.145Z\",\r\n  " +
                "\"finishedAt\": \"2021-06-29T16:00:54.145Z\"\r\n}", 
                Encoding.UTF8, "application/json");

            await _httpClient.PostAsync(_baseUri + "api/tasks", content);

            content = new StringContent(
                "{\r\n  " +
                "\"id\": 0,\r\n  " +
                "\"name\": " +
                "\"string\",\r\n  " +
                "\"createdAt\": \"2021-06-29T16:01:01.209Z\"\r\n}",
                Encoding.UTF8, "application/json");

            await _httpClient.PostAsync(_baseUri + "api/teams", content);

            content = new StringContent(
                "{\r\n  " +
                "\"id\": 0,\r\n  " +
                "\"teamId\": 0,\r\n  " +
                "\"firstName\": \"Elon\",\r\n  " +
                "\"lastName\": \"Musk\",\r\n  " +
                "\"email\": \"test.email@gmail.com\",\r\n  " +
                "\"registeredAt\": \"2021-06-29T16:01:08.056Z\",\r\n  " +
                "\"birthDay\": \"2001-06-29T16:01:08.056Z\"\r\n}", 
                Encoding.UTF8, "application/json");

            await _httpClient.PostAsync(_baseUri + "api/users", content);
        }

        public string GetProjectTaskCountByUser(int id)
        {
            try
            {
                _projectTaskCountByUserAsync = _httpClient.GetAsync(_baseUri + $"api/custom/project_task_by_user/{id}");
            }
            catch
            {
                throw new HttpRequestException("Can't get response.");
            }

            return _projectTaskCountByUserAsync.Result.Content.ReadAsStringAsync().Result;
        }

        public string GetTasksByUserLess45(int id)
        {
            try
            {
                _tasksByUserLess45Async = _httpClient.GetAsync(_baseUri + $"api/Custom/tasks_by_user/{id}");
            }
            catch
            {
                throw new HttpRequestException("Can't get response.");
            }

            return _tasksByUserLess45Async.Result.Content.ReadAsStringAsync().Result;
        }

        public string GetTasksByUserDone(int id)
        {
            try
            {
                _taskNamesByUserDone = _httpClient.GetAsync(_baseUri + $"tasks_by_user_done/{id}");
            }
            catch
            {
                throw new HttpRequestException("Can't get response.");
            }
           
            return _taskNamesByUserDone.Result.Content.ReadAsStringAsync().Result;
        }

        public string GetTeamsWithUsersOlder10()
        {
            try
            {
                _teamsWithUsersOlder10 = _httpClient.GetAsync(_baseUri + $"api/Custom/teams_users");
            }
            catch
            {
                throw new HttpRequestException("Can't get response.");
            }

            return _teamsWithUsersOlder10.Result.Content.ReadAsStringAsync().Result;
        }

        public string GetUsersWithTasks()
        {
            try
            {
                _usersWithTasks = _httpClient.GetAsync(_baseUri + $"api/Custom/users_task");
            }
            catch
            {
                throw new HttpRequestException("Can't get response.");
            }
            
            return _usersWithTasks.Result.Content.ReadAsStringAsync().Result;
        }

        public string GetUserStruct(int id)
        {
            try
            {
                _userStruct = _httpClient.GetAsync(_baseUri + $"api/Custom/user_structs/{id}");
            }
            catch
            {
                throw new HttpRequestException("Can't get response.");
            }
            
            return _userStruct.Result.Content.ReadAsStringAsync().Result;
        }

        public string GetProjectStruct()
        {
            try
            {
                _projectStructs = _httpClient.GetAsync(_baseUri + $"api/Custom/projects_structs");
            }
            catch
            {
                throw new HttpRequestException("Can't get response.");
            }

            return _projectStructs.Result.Content.ReadAsStringAsync().Result;
        }


    }
}
