﻿using BinaryTask3.Common.DTOs;

namespace BinaryTask3.Common.CustomModels
{
    public class TaskInfo : TaskDTO
    {
        public UserDTO Performer { get; set; }
    }
}
