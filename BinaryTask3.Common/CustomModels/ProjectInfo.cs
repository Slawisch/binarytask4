﻿using System.Collections.Generic;
using BinaryTask3.Common.DTOs;

namespace BinaryTask3.Common.CustomModels
{
    public class ProjectInfo : ProjectDTO
    {
        public TeamDTO Team { get; set; }
        public UserDTO Author { get; set; }
        public IEnumerable<TaskInfo> Tasks;
    }
}
