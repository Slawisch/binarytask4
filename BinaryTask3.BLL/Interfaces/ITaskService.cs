﻿using System.Collections.Generic;
using BinaryTask3.Common.DTOs;

namespace BinaryTask3.BLL.Interfaces
{
    public interface ITaskService
    {
        IEnumerable<TaskDTO> GetTasks();
        TaskDTO GetTask(int id);
        void UpdateTask(int id, TaskDTO task);
        void CreateTask(TaskDTO task);
        void DeleteTask(int id);
    }
}
