﻿using System.Collections.Generic;
using BinaryTask3.Common.DTOs;

namespace BinaryTask3.BLL.Interfaces
{
    public interface IUserService
    {
        IEnumerable<UserDTO> GetUsers();
        UserDTO GetUser(int id);
        void UpdateUser(int id, UserDTO user);
        void CreateUser(UserDTO user);
        void DeleteUser(int id);
    }
}
