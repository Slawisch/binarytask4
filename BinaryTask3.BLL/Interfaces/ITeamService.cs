﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BinaryTask3.Common.DTOs;

namespace BinaryTask3.BLL.Interfaces
{
    public interface ITeamService
    {
        IEnumerable<TeamDTO> GetTeams();
        TeamDTO GetTeam(int id);
        void UpdateTeam(int id, TeamDTO team);
        void CreateTeam(TeamDTO team);
        void DeleteTeam(int id);
    }
}
