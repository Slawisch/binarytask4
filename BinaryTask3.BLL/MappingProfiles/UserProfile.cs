﻿using AutoMapper;
using BinaryTask3.Common.DTOs;
using BinaryTask3.DAL.Entities;

namespace BinaryTask3.BLL.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>();
        }
    }
}
