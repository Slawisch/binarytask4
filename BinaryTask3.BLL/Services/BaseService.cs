﻿using AutoMapper;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;

namespace BinaryTask3.BLL.Services
{
    public abstract class BaseService
    {
        private protected readonly IMapper Mapper;
        private protected readonly IUnitOfWork UnitOfWork;

        public BaseService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            UnitOfWork = unitOfWork;
            Mapper = mapper;
        }
    }
}
