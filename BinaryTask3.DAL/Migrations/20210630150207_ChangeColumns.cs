﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BinaryTask3.DAL.Migrations
{
    public partial class ChangeColumns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FinishedAt",
                table: "Tasks",
                newName: "FinishedAtDate");

            migrationBuilder.RenameColumn(
                name: "CreatedAt",
                table: "Tasks",
                newName: "CreatedAtDate");

            migrationBuilder.RenameColumn(
                name: "Deadline",
                table: "Projects",
                newName: "DeadlineDate");

            migrationBuilder.RenameColumn(
                name: "CreatedAt",
                table: "Projects",
                newName: "CreatedAtDate");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FinishedAtDate",
                table: "Tasks",
                newName: "FinishedAt");

            migrationBuilder.RenameColumn(
                name: "CreatedAtDate",
                table: "Tasks",
                newName: "CreatedAt");

            migrationBuilder.RenameColumn(
                name: "DeadlineDate",
                table: "Projects",
                newName: "Deadline");

            migrationBuilder.RenameColumn(
                name: "CreatedAtDate",
                table: "Projects",
                newName: "CreatedAt");
        }
    }
}
