﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BinaryTask3.DAL.Migrations
{
    public partial class RemoveNestedTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Teams_TeamId",
                table: "Projects");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Projects_ProjectId",
                table: "Tasks");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Users_UserId",
                table: "Tasks");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Teams_TeamId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_TeamId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks");

            migrationBuilder.DropIndex(
                name: "IX_Tasks_UserId",
                table: "Tasks");

            migrationBuilder.DropIndex(
                name: "IX_Projects_TeamId",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Tasks");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAtDate", "DeadlineDate" },
                values: new object[] { new DateTime(2021, 7, 2, 14, 15, 33, 411, DateTimeKind.Local).AddTicks(9102), new DateTime(2021, 7, 7, 14, 15, 33, 411, DateTimeKind.Local).AddTicks(9344) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAtDate",
                value: new DateTime(2021, 7, 2, 14, 15, 33, 411, DateTimeKind.Local).AddTicks(7740));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 2, 14, 15, 33, 411, DateTimeKind.Local).AddTicks(6923));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                column: "BirthDay",
                value: new DateTime(1999, 7, 2, 14, 15, 33, 409, DateTimeKind.Local).AddTicks(4507));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(1991, 7, 2, 14, 15, 33, 411, DateTimeKind.Local).AddTicks(5992), new DateTime(2021, 6, 17, 14, 15, 33, 411, DateTimeKind.Local).AddTicks(6022) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Tasks",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAtDate", "DeadlineDate" },
                values: new object[] { new DateTime(2021, 7, 2, 13, 55, 12, 895, DateTimeKind.Local).AddTicks(6463), new DateTime(2021, 7, 7, 13, 55, 12, 895, DateTimeKind.Local).AddTicks(6704) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAtDate",
                value: new DateTime(2021, 7, 2, 13, 55, 12, 895, DateTimeKind.Local).AddTicks(5101));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 2, 13, 55, 12, 895, DateTimeKind.Local).AddTicks(4294));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                column: "BirthDay",
                value: new DateTime(1999, 7, 2, 13, 55, 12, 893, DateTimeKind.Local).AddTicks(2778));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(1991, 7, 2, 13, 55, 12, 895, DateTimeKind.Local).AddTicks(3374), new DateTime(2021, 6, 17, 13, 55, 12, 895, DateTimeKind.Local).AddTicks(3403) });

            migrationBuilder.CreateIndex(
                name: "IX_Users_TeamId",
                table: "Users",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_UserId",
                table: "Tasks",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_TeamId",
                table: "Projects",
                column: "TeamId");

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Teams_TeamId",
                table: "Projects",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Projects_ProjectId",
                table: "Tasks",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Users_UserId",
                table: "Tasks",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Teams_TeamId",
                table: "Users",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
