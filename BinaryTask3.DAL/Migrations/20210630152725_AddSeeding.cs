﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BinaryTask3.DAL.Migrations
{
    public partial class AddSeeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 1, new DateTime(2021, 6, 30, 18, 27, 24, 893, DateTimeKind.Local).AddTicks(9335), "Team1" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { 1, new DateTime(1999, 6, 30, 18, 27, 24, 891, DateTimeKind.Local).AddTicks(7381), "EmailText", "UserFirstName1", "UserLastName1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAtDate", "DeadlineDate", "Description", "Name", "TeamId" },
                values: new object[] { 1, 1, new DateTime(2021, 6, 30, 18, 27, 24, 894, DateTimeKind.Local).AddTicks(1531), new DateTime(2021, 7, 5, 18, 27, 24, 894, DateTimeKind.Local).AddTicks(1776), null, null, 1 });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { 2, new DateTime(1991, 6, 30, 18, 27, 24, 893, DateTimeKind.Local).AddTicks(8405), "Email@gmail.com", "UserFirstName2", "UserLastName2", new DateTime(2021, 6, 15, 18, 27, 24, 893, DateTimeKind.Local).AddTicks(8436), 1 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAtDate", "Description", "FinishedAtDate", "Name", "PerformerId", "ProjectId", "State", "UserId" },
                values: new object[] { 1, new DateTime(2021, 6, 30, 18, 27, 24, 894, DateTimeKind.Local).AddTicks(152), null, null, null, 2, 1, 0, null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
