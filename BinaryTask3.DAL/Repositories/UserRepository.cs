﻿using System;
using System.Collections.Generic;
using BinaryTask3.DAL.EF;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;

namespace BinaryTask3.DAL.Repositories
{
    class UserRepository : IRepository<User>
    {
        private readonly ProjectsContext _db;

        public UserRepository(ProjectsContext db)
        {
            this._db = db;
        }

        public IEnumerable<User> Read()
        {
            return _db.Users;
        }

        public User Read(int id)
        {
            if (_db.Users.Find(id) != null)
                return _db.Users.Find(id);
            throw new ArgumentException($"User with id({id}) not found");
        }

        public void Update(int id, User user)
        {
            Delete(id);
            Create(user);
        }

        public void Create(User user)
        {
            if (_db.Users.Find(user.Id) == null)
                _db.Users.Add(user);
            else
                throw new ArgumentException($"User with id({user.Id}) already exists");
        }

        public void Delete(int id)
        {
            var userForDelete = _db.Users.Find(id);
            if (userForDelete != null)
                _db.Users.Remove(userForDelete);
            else
                throw new ArgumentException($"User with id({id}) not found");
        }
    }
}
