﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BinaryTask3.DAL.EF;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;

namespace BinaryTask3.DAL.Repositories
{
    public class TaskRepository : IRepository<TaskEntity>
    {
        private readonly ProjectsContext _db;

        public TaskRepository(ProjectsContext db)
        {
            this._db = db;
        }

        public IEnumerable<TaskEntity> Read()
        {
            return _db.Tasks;
        }

        public TaskEntity Read(int id)
        {
            if (_db.Teams.Find(id) != null)
                return _db.Tasks.Find(id);
            throw new ArgumentException($"Task with id({id}) not found");
        }

        public void Update(int id, TaskEntity task)
        {
            Delete(id);
            Create(task);
        }

        public void Create(TaskEntity task)
        {
            if (_db.Tasks.Find(task.Id) == null)
                _db.Tasks.Add(task);
            else
                throw new ArgumentException($"Task with id({task.Id}) already exists");
        }

        public void Delete(int id)
        {
            var taskForDelete = _db.Tasks.Find(id);
            if (taskForDelete != null)
                _db.Tasks.Remove(taskForDelete);
            else
                throw new ArgumentException($"Task with id({id}) not found");
        }
    }
}
