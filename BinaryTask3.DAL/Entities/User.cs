﻿using System;
using System.ComponentModel.DataAnnotations;

#nullable enable
namespace BinaryTask3.DAL.Entities
{
    public class User : BaseEntity
    {
        public int? TeamId { get; set; }
        [StringLength(20)]
        public string? FirstName { get; set; }
        [StringLength(20)]
        public string? LastName { get; set; }

        [DataType(DataType.EmailAddress)]
        public string? Email { get; set; }
        [Required]
        public DateTime RegisteredAt { get; set; }
        [Required] 
        public DateTime BirthDay { get; set; }

    }
}
