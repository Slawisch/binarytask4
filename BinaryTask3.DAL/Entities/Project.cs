﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable enable

namespace BinaryTask3.DAL.Entities
{
    public class Project : BaseEntity
    {
        [Required] 
        public int AuthorId { get; set; }
        [Required]
        public int TeamId { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        [Required]
        public DateTime DeadlineDate { get; set; }
        [Required]
        public DateTime CreatedAtDate { get; set; }
    }
}
