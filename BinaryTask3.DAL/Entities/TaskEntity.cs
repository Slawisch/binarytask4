﻿using System;
using System.ComponentModel.DataAnnotations;

#nullable enable

namespace BinaryTask3.DAL.Entities
{
    public class TaskEntity : BaseEntity
    {
        [Required]
        public int ProjectId { get; set; }
        [Required]
        public int PerformerId { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        [Required]
        public TaskState State { get; set; }
        [Required]
        public DateTime CreatedAtDate { get; set; }
        public DateTime? FinishedAtDate { get; set; }
    }
}
