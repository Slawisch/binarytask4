﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BinaryTask3.BLL.Interfaces;
using BinaryTask3.Common.DTOs;

namespace BinaryTask3.WebAPI.Controllers
{
    [ApiController]
    [Route("api/Users")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public IEnumerable<UserDTO> GetUsers()
        {
            return _userService.GetUsers();
        }

        [HttpGet("{id}")]
        public IActionResult GetUser(int id)
        {
            try
            {
                return new JsonResult(_userService.GetUser(id));
            }
            catch (ArgumentException)
            {
                return new NotFoundResult();
            }

        }

        [HttpPost]
        public IActionResult AddUser(UserDTO user)
        {
            try
            {
                _userService.CreateUser(user);
                return new OkResult();
            }
            catch (ArgumentException)
            {
                return new ConflictResult();
            }

        }

        [HttpDelete]
        public IActionResult DeleteUser(int id)
        {
            try
            {
                _userService.DeleteUser(id);
                return new OkResult();
            }
            catch (ArgumentException)
            {
                return new NotFoundResult();
            }

        }

        [HttpPut]
        public IActionResult UpdateUser(int id, UserDTO user)
        {
            try
            {
                _userService.UpdateUser(id, user);
                return new OkResult();
            }
            catch (ArgumentException)
            {
                return new NotFoundResult();
            }

        }
    }
}
